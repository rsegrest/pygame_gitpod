import pygame
from Rank import Rank
from Suit import Suit

class Card:
    def __init__(self, rank, suit, show=False):
        super().__init__()
        self.rank = rank
        self.suit = suit
        self.show = show
        self.graphic = pygame.image.load("images/cards/"+str(rank)+"_"+str(suit)+".png")
        self.back = pygame.image.load("images/cards/CARD_BACK.png")

    def setShowing(self,show=True):
        self.show = show

    def getFullSizedGraphic(self):
        if (self.show):
            return self.graphic
        else:
            return self.back

    def getMediumSizedGraphic(self):
        graphic = self.back
        if (self.show):
            graphic = self.graphic
        med_graphic = pygame.transform.scale(graphic, (153,215))
        return med_graphic
    
    def getSmallSizedGraphic(self):
        graphic = self.back
        if (self.show):
            graphic = self.graphic
        sm_graphic = pygame.transform.scale(graphic, (77,107))
        return sm_graphic

    def __str__(self):
        showing = "(face down)"
        if (self.show):
            showing = "(face UP)"
        return (str(self.rank) + " of " + str(self.suit) + " " + showing)
    
    def __repr__(self):
        return self.__str__()
