import pygame

pygame.init()
btn_font = pygame.font.SysFont("arial", 20)

RED = (255,0,0)
GRAY = (128,128,128)

class Button:
    def __init__(self, label, func, pos):
        super().__init__()
        self.label = label
        self.func = func
        self.size = (100,100)
        self.pos = pos
        self.active = True
        self.visible = True
    
    def draw(self,win):
        color = RED
        if self.active == False: color = GRAY
        if self.visible:
            pygame.draw.rect(win,color,(*self.pos,*self.size),0)
            label = btn_font.render(self.label, 1, (0,0,0))
            label_x = (self.pos[0]+(self.size[0]/2)-(label.get_width()/2))
            label_y = (self.pos[1]+(self.size[1]/2)-(label.get_height()/2))
            win.blit(label, (label_x, label_y))

    def isOver(self,pos):
        if pos[0] > self.pos[0] and pos[0] < self.pos[0] + self.size[0]:
            if pos[1] > self.pos[1] and pos[1] < self.pos[1] + self.size[1]:
                self.clickEvent()
                return True
        return False

    def setIsActive(self, active=True):
        self.active = active
    
    def setIsVisible(self,visible=True):
        print('setting button not visible')
        self.visible = visible

    def clickEvent(self):
        if self.active:
            self.func()
        else:
            print("INACTIVE BUTTON!")