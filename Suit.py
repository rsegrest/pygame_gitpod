from enum import Enum

# print(Suit.Suit.SPADES)
# -> Suit.SPADES

# Using enum class create enumerations
class Suit(Enum):
    SPADES = 1
    HEARTS = 2
    CLUBS = 3
    DIAMONDS = 4

    def __str__(self):
        return str(self.name)
    
    def get_emoji(self):
        if self.name == 'SPADES':
            return '\U00002660'
        elif self.name == 'HEARTS':
            return '\U00002665'
        elif self.name == 'CLUBS':
            return '\U00002663'
        elif self.name == 'DIAMONDS':
            return '\U00002666'