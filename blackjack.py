#########################################################
## File Name: blackjack.py                             ##
#########################################################
import pygame
import random
from Rank import Rank
from Suit import Suit
from Card import Card
from Chip import Chip
from Result import Result
from Button import Button
from GraphicButton import GraphicButton
from Deck import Deck
from BlackjackLogic import BlackjackLogic
import time

pygame.init()
winHeight = 800
winWidth = 1200
win=pygame.display.set_mode((winWidth,winHeight))
#---------------------------------------#
# initialize global variables/constants #
#---------------------------------------#
BLACK = (0,0, 0)
WHITE = (255,255,255)
RED = (255,0, 0)
GREEN = (0,128,0)
LIGHT_GREEN = (128,255,128)
BLUE = (0,0,255)
LIGHT_BLUE = (102,255,255)
CREAM = (200,200,255)


CARD_SIZE = (142,200)

dealer_y_pos = win.get_height() * .1
player_y_pos = win.get_height() * .9 - CARD_SIZE[1]

btn_font = pygame.font.SysFont("arial", 20)
guess_font = pygame.font.SysFont("monospace", 24)
lost_font = pygame.font.SysFont('arial', 45)

buttons = []
chips = []

bj = BlackjackLogic()



global currentMessage
currentMessage = "TEST"
message_x = 600
message_y = 600
message_width = 600
message_height = 100

# message_label = btn_font.render(currentMessage,1, BLACK)
message_rect = pygame.Rect((win.get_width()/2,(win.get_height()*0.5)),((0.5)*win.get_width(),400))

def show_message(message_text): # (x,y):
    message = btn_font.render(message_text, True, BLACK)
    win.blit(message, message_rect)

bet_label_text = "Bet : "+str(bj.bets[1])

player_label = btn_font.render("Player", 1, BLACK)
dealer_label = btn_font.render("Dealer", 1, BLACK)
bet_label = btn_font.render(bet_label_text, 1, BLACK)

player_rect = ((win.get_width()/2)-(player_label.get_width()/2), (0.6)*win.get_height())
dealer_rect = ((win.get_width()/2)-(dealer_label.get_width()/2), (0.0625)*win.get_height())
bet_rect = ((win.get_width()*.85), (0.6)*win.get_height())
buttons = []

# update message
def updateMessage(newMessage):
    global currentMessage
    currentMessage = newMessage

def updateBet(newBet):
    global bet_label_text
    bet_label_text = "Bet : "+str(newBet)

def deal():
    print('deal')
    bj.dealGame()
    if can_split(bj.hands[1]):
        split_btn.setIsVisible(True)
    # Dealer card showing is ACE, offer insurance
    if bj.hands[0][0].rank == Rank.ACE:
        setInsuranceButtonActive(True)
    if bj.evaluateHand(bj.hands[1]) == 21:
        print("player has 21")
        hit_btn.setIsActive(False)
        stand_btn.setIsActive(False)
        double_down_btn.setIsVisible(False)
        stand()

def doubleDown():
    global currentMessage
    double_down_btn.setIsVisible(False)
    bj.hit()
    if(bj.checkIfBusted(bj.hands[1])):
        bust()
    else:
        updateMessage("Player Doubles Down!")
    bj.advanceTurn()
    bj.dealerMove()
    endRound()
    redraw_game_window()

def bust():
    updateMessage("BUST!")
    setActionButtonsActive(False)
    bj.advanceTurn()
    bj.dealerMove()


# player hits
def hit():
    global currentMessage
    print('insurance_btn -- setting not visible')
    insurance_btn.setIsVisible(False)
    double_down_btn.setIsVisible(False)
    split_btn.setIsVisible(False)
    bj.hit()
    if(bj.checkIfBusted(bj.hands[1])):
        bust()
    elif(bj.evaluateHand(bj.hands[1]) == 21):
        hit_btn.setIsActive(False)
        stand_btn.setIsActive(False)
        stand()
    else:
        updateMessage("Player Hits!")
    redraw_game_window()


# player stands
def stand():
    global currentMessage
    insurance_btn.setIsVisible(False)
    double_down_btn.setIsVisible(False)
    split_btn.setIsVisible(False)
    print("STAND")
    bj.stand()
    updateMessage("Player Stands.")
    setActionButtonsActive(False)
    redraw_game_window()

    bj.advanceTurn()
    bj.dealerMove()
    endRound()

def betAgain():
    print("bet again!")
    updateMessage("...")
    setupGame()

def insurance():
    # insurance bet is half original bet, pays 2:1 on dealer 21
    insurance_btn.setIsVisible(False)
    print("Insurance")

def split():
    split_btn.setIsVisible(False)
    print("Split")

def chip_action(amount):
    bj.adjustBet(amount)
    updateBet(str(bj.bets[1]))

def chip_reduce(amount):
    bj.adjustBet(-1*amount)
    updateBet(str(bj.bets[1]))

hit_btn = Button("Hit", hit, (win.get_width() * .9, win.get_height() * .7))
buttons.append(hit_btn)

stand_btn = Button("Stand", stand, (win.get_width() * .85, win.get_height() * .85))
buttons.append(stand_btn)

double_down_btn = Button("Double\nDown", doubleDown, (win.get_width() * .75, win.get_height() * .7))
buttons.append(double_down_btn)

split_btn = Button("Split", split, (win.get_width() * .7, win.get_height() * .85))
buttons.append(split_btn)

bet_again_btn = Button("Bet Again", betAgain, (win.get_width() * .9, win.get_height() * .1))
buttons.append(bet_again_btn)

insurance_btn = Button("Insurance?", insurance, (win.get_width() * .9, win.get_height() * .25))
insurance_btn.setIsVisible(False)
buttons.append(insurance_btn)

deal_btn = Button("Deal", deal, (win.get_width() * .9, win.get_height() * .4))
deal_btn.setIsVisible(True)
buttons.append(deal_btn)


# TODO
def endRound():
    print('check:')
    result = bj.checkResults()
    print(result[1])
    if result[1] == Result.WIN:
        updateMessage("PLAYER WINS! with "+str(bj.evaluateHand(bj.hands[1])))
    elif result[1] == Result.PUSH:
        updateMessage("PUSH at "+str(bj.evaluateHand(bj.hands[1])))
    elif result[1] == Result.BLACKJACK:
        updateMessage("PLAYER HAS BLACKJACK!")
    elif result[1] == Result.LOSE:
        updateMessage("PLAYER LOSES with "+str(bj.evaluateHand(bj.hands[1])))
    bet_again_btn.setIsActive(True)

# MOVED
def render_card(card, pos):
    card_img = card.getMediumSizedGraphic()
    win.blit(card_img, (pos[0],pos[1]))

def getChipPosition(chip):
    if chip.name == 'CHIPS_50':
        return (win.get_width() * .05,win.get_height()*.75)
    elif chip.name == 'CHIPS_100':
        return (win.get_width() * .15,win.get_height()*.75)
    elif chip.name == 'CHIPS_500':
        return (win.get_width() * .25,win.get_height()*.75)
    elif chip.name == 'CHIPS_1000':
        return (win.get_width() * .1,win.get_height()*.85)
    elif chip.name == 'CHIPS_5000':
        return (win.get_width() * .2,win.get_height()*.85)

def getNegChipPosition(chip):
    if chip.name == 'CHIPS_50':
        return (win.get_width() * .1,win.get_height()*.8)
    elif chip.name == 'CHIPS_100':
        return (win.get_width() * .2,win.get_height()*.8)
    elif chip.name == 'CHIPS_500':
        return (win.get_width() * .3,win.get_height()*.8)
    elif chip.name == 'CHIPS_1000':
        return (win.get_width() * .15,win.get_height()*.9)
    elif chip.name == 'CHIPS_5000':
        return (win.get_width() * .25,win.get_height()*.9)

# MOVED
def render_chip(chip):
    chip_img = chip.getMediumSizedGraphic()
    chip_btn = GraphicButton(chip_img, lambda: chip_action(chip.value), getChipPosition(chip))
    buttons.append(chip_btn)

# MOVED
def render_negative_chip(chip):
    chip_img = chip.getSmallGraphic()
    chip_btn = GraphicButton(chip_img, lambda: chip_reduce(chip.value), getNegChipPosition(chip))
    buttons.append(chip_btn)

# MOVED
def get_card_pos(card_num, is_dealer=True):
    x = ((win.get_width() / 2) - (100 + card_num*40)) + card_num*80
    y = dealer_y_pos
    if (not is_dealer):
        y = player_y_pos
    return (x,y)

def drawBetLabel():
    # bet_label_text = "Bet :"
    bet_label = btn_font.render(bet_label_text, 1, BLACK)
    win.blit(bet_label, bet_rect)

def drawDealerLabel():
    dealer_hand = bj.hands[0]
    dealer_turn = (bj.currentPTurn == 0)
    dealer_label_text = "Dealer"
    if (dealer_turn):
        dealer_label_text = "Dealer "+str(bj.evaluateHand(bj.hands[0]))
        if bj.checkIfBlackjack(dealer_hand):
            dealer_label_text = "Dealer has Blackjack!"
        if bj.checkIfBusted(dealer_hand):
            dealer_label_text = "Dealer BUSTS!"
    dealer_label = btn_font.render(dealer_label_text, 1, BLACK)
    win.blit(dealer_label, dealer_rect)

# MOVED
def drawPlayerLabel():
    player_label = btn_font.render("Player "+str(bj.evaluateHand(bj.hands[1])), True, BLACK)
    win.blit(player_label, player_rect)

# MOVED
def render_dealer_cards(hand):
    drawDealerLabel()
    for i in range(len(hand)):
        render_card(card=hand[i], pos=get_card_pos(i))

# MOVED
def render_player_cards(hand):
    drawPlayerLabel()
    drawBetLabel()

    for i in range(len(hand)):
        render_card(card=hand[i], pos=get_card_pos(i, False))

# MOVED
def render_chips():
    chiptypes = [Chip.CHIPS_50,Chip.CHIPS_100,Chip.CHIPS_500,Chip.CHIPS_1000,Chip.CHIPS_5000]
    for c in chiptypes:
        render_chip(c)
        render_negative_chip(c)

def redraw_game_window():
    win.fill(GREEN)
    
    if bj.roundActive:
        render_dealer_cards(bj.hands[0])
        render_player_cards(bj.hands[1])
    else:
        render_chips()

    drawButtons()
    show_message(currentMessage)
    pygame.display.update()

def drawButtons():
    for b in buttons:
        b.draw(win)

def setActionButtonsActive(active=True):
    hit_btn.setIsActive(active)
    stand_btn.setIsActive(active)

def setDoubleDownButtonActive(visible=True):
    double_down_btn.setIsVisible(visible)

def setInsuranceButtonActive(visible=True):
    insurance_btn.setIsVisible(visible)
    # drawButtons()

def buttonHit(x, y):
    for b in buttons:
        if b.isOver((x,y)):
            return True
    return None

def reset():
    pass

def can_split(hand):
    return hand[0].rank == hand[1].rank

# New Hand
def setupGame():
    split_btn.setIsVisible(False)
    insurance_btn.setIsVisible(False)
    double_down_btn.setIsVisible(True)
    setActionButtonsActive(True)
    bj.setupGame()
    

#MAINLINE

setupGame()
print("Beginning while loop:")
inPlay = True
while inPlay:
    redraw_game_window()
    
    for event in pygame.event.get():
        
        if event.type == pygame.QUIT:
            inPlay = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                inPlay = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            clickPos = pygame.mouse.get_pos()
            buttonHit(clickPos[0], clickPos[1])
    

pygame.quit()

# always quit pygame when done!