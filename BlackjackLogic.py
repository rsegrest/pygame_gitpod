from Card import Card
from Rank import Rank
from Suit import Suit
from Result import Result
from Deck import Deck
import random
import time

class BlackjackLogic:
    deck = Deck()
    hands = []
    results = []
    # cents
    balances = [0, 10000000]
    bets = [0,0]

    numPlayers = 2
    currentPTurn = 1
    activeDeck = None
    roundActive = False

    # calls ready functions
    #   Creates a new shuffled deck
    #   Blanks out the hands and results of last round
    #   Deals
    def setupGame(self):
        print('bj.setupGame()')
        # self.waitForBets()
        self.activeDeck = Deck.getShuffledDeck()
        self.initGame()

    def initGame(self):
        self.hands = []
        self.results = []
        for i in range(self.numPlayers):
            self.hands.append([])
            self.results.append([])
    
    def advanceTurn(self):
        print('bj.advanceTurn')
        self.currentPTurn += 1
        if self.currentPTurn >= self.numPlayers:
            print("setting dealer turn")
            self.currentPTurn = 0 # DEALER
        # TEMP?
        self.dealerMove()
        print("Dealer move is complete.")
    
    def hit(self):
        print("bj.hit")
        card = self.activeDeck.pop()
        card.setShowing()
        self.hands[self.currentPTurn].append(card)

    def stand(self):
        print("STAND")
    
    def inputPlayerOptions(self):
        print("Current player turn: " + str(self.currentPTurn))
        k = input("(H)it or (S)tand ?:")
        
        if (k == "h") or (k == "H"):
            self.hit()
            if (self.checkIfBlackjack(self.hands[self.currentPTurn])):
                print("BLACKJACK!")
                self.results[self.currentPTurn].append(Result.BLACKJACK)
                self.stand()
            elif (self.checkIfBusted(self.hands[self.currentPTurn])):
                print("BUSTED!")
                self.results[self.currentPTurn].append(Result.LOSE)
                self.stand()
            elif (self.evaluateHand(self.hands[self.currentPTurn]) == 21):
                self.stand()

        elif (k == "s") or (k == "S"):
            self.stand()
        else:
            print("UNKNOWN")
        self.printTable()

    def waitForBets(self):
        bet = input('Bet?')
        self.bets[1] = int(bet)*100
        print('Bets are set at:')
        print((int(self.bets[1])/100))


    def initBet(self, bet, player=1):
        self.bets[player] = int(bet)*100
    
    def adjustBet(self, bet, player=1):
        self.bets[player] += bet
        if self.bets[player] < 0:
            self.bets[player] = 0

    def payoutWin(self, bet, player=1):
        self.payout(self, bet, player)

    def payoutLoss(self, bet, player=1):
        self.payout(self, (-1*bet), player)

    def payoutBlackjack(self, bet, player=1):
        self.payout(self, (1.5*bet), player)
    
    def payout(self, payout, player=1):
        self.balances[player] += payout
    
    def settleUp(self):
        if self.results[1] == Results.BLACKJACK:
            print('Player got BLACKJACK!')
            self.balances[1] += self.bets[1]*2.5
        elif self.results[1] == Results.WIN:
            print('Player WON! ')
            self.balances[1] += self.bets[1]*2
        elif self.results[1] == Results.PUSH:
            print('push...')
            self.balances[1] += self.bets[1]
        else:
            print('Player loses')
            
    
    def dealGame(self,deck=None):
        print('bj.dealGame')
        if (deck == None):
            deck = self.activeDeck
        self.roundActive = True
        for i in range(2):
            for j in range(self.numPlayers):
                card = deck.pop()
                if (j != 0):
                    card.setShowing()
                elif (i != 1):
                    card.setShowing()
                self.hands[j].append(card)
        self.printTable()
        self.currentPTurn = 1
    
    def checkWhoHasBlackjack(self):
        blackjackList = [False]
        print("checking if BLACKJACK:")
        for k in range(1, len(self.hands)):
            if self.checkIfBlackjack(self.hands[k]):
                print("player "+str(k)+" has black jack!")
                blackjackList.append(True)
            else:
                blackjackList.append(False)
        if self.checkIfBlackjack(self.hands[0]):
            print("Dealer has blackjack!")
            blackjackList.append(True)
        return blackjackList

    def printTable(self):
        for h in self.hands:
            self.printHand(h)
    
    def printHand(self,hand):
        print("HAND: " + str(hand) + ", VALUE: " + str(self.evaluateHand(hand)))

    def printResults(self):
        for i in range(self.numPlayers):
            str(self.printResult(i))
        print()

    def printResult(self,playerNum):
        print("PLAYER "+str(playerNum)+" RESULT: " + str(self.results[playerNum]))

    def dealerMove(self):
        print('dealerMove')
        self.hands[0][1].setShowing()
        if(self.evaluateHand(self.hands[0]) < 17):
            self.hit()
        else:
            self.stand()
        self.printTable()
        # self.finishRound()

    def finishRound(self):
        print('bj.finishRound')
        time.sleep(3)
        # TODO: Later should give the option here to bet or leave
        # self.startNewRound()
        self.setupGame()

    # UNNEEDED?
    # def startNewRound(self):
    #     self.initGame()
    #     self.dealGame()

    def checkResults(self):
        print("checking results:")
        dealerScore = self.evaluateHand(self.hands[0])
        for i in range(1, self.numPlayers):
            playerScore = self.evaluateHand(self.hands[i])
            if (playerScore <= 21):
                if self.checkIfBusted(self.hands[0]):
                    self.results[i] = Result.WIN
                elif (playerScore == dealerScore):
                    self.results[i] = Result.PUSH
                elif (playerScore > dealerScore):
                    self.results[i] = Result.WIN
                elif (playerScore < dealerScore):
                    self.results[i] = Result.LOSE
            else:
                self.results[i] = Result.LOSE
        return self.results

    def advanceGame(self):
        if(self.currentPTurn == 0):
            self.dealerMove()
            self.checkResults()
            self.printResults()
            self.setupGame()
        else:
            self.inputPlayerOptions()

    def hitHand(self,hand):
        card = deck.pop()
        self.hand.append(card)

    def checkNumAces(self,hand):
        numAces = 0
        for c in hand:
            if c.rank == Rank.ACE:
                numAces += 1
        return numAces
    
    def handHasTenValueCard(self,hand):
        for c in hand:
            if (c.rank == Rank.KING) or (c.rank == Rank.QUEEN) \
                or (c.rank == Rank.JACK) or (c.rank == Rank.TEN):
                    return True
        return False

    def checkIfBlackjack(self,hand):
        if len(hand) == 2:
            if (self.checkNumAces(hand) == 1) and (self.handHasTenValueCard(hand)):
                return True
        return False
    
    def checkIfBusted(self,hand):
        if self.evaluateHand(hand) > 21:
            return True
        return False

    def evaluateHand(self,hand):
        handValue = 0
        i = self.checkNumAces(hand)
        for c in hand:
            handValue += self.getCardValue(c)
            if handValue > 0:
                while(i > 0 and handValue > 21):
                    i -= 1
                    handValue -= 10
        return handValue
    
    def getCardValue(self, card):
        if card.rank == Rank.ACE: return 11
        if (card.rank == Rank.KING) or (card.rank == Rank.QUEEN) or (card.rank == Rank.JACK) or (card.rank == Rank.TEN):
            return 10
        if (card.rank == Rank.NINE): return 9
        if (card.rank == Rank.EIGHT): return 8
        if (card.rank == Rank.SEVEN): return 7
        if (card.rank == Rank.SIX): return 6
        if (card.rank == Rank.FIVE): return 5
        if (card.rank == Rank.FOUR): return 4
        if (card.rank == Rank.THREE): return 3
        if (card.rank == Rank.TWO): return 2

    def getRoundActive(self):
        return self.roundActive

    def __init__(self):
        self.cards = Deck.getStandardDeck()
    
    def __str__(self):
        return str(self.cards)

    def getCards(self):
        print(self.cards)


def main():
    bj = BlackjackLogic()
    bj.setupGame()
    while(bj.getRoundActive()):
        bj.advanceGame()

if __name__=="__main__":
    main()