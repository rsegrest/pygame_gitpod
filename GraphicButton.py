import pygame

pygame.init()
btn_font = pygame.font.SysFont("arial", 20)

RED = (255,0,0)
GRAY = (128,128,128)

class GraphicButton:
    def __init__(self, graphic, func, pos):
        super().__init__()
        self.graphic = graphic
        self.func = func
        self.size = (graphic.get_width(),graphic.get_height())
        self.pos = pos
        self.active = True
        self.visible = True
    
    def draw(self,win):
        # pygame.draw.rect(win,color,(*self.pos,*self.size),0)
        if self.visible:
            win.blit(self.graphic, self.pos)

    def isOver(self,pos):
        if pos[0] > self.pos[0] and pos[0] < self.pos[0] + self.size[0]:
            if pos[1] > self.pos[1] and pos[1] < self.pos[1] + self.size[1]:
                self.clickEvent()
                return True
        return False

    def setIsActive(self, active=True):
        self.active = active

    # def setShowing(self,show=True):
    #     self.show = show
        
    def setIsVisible(self,visible=True):
        print('setting button not visible')
        self.visible = visible


    def clickEvent(self):
        if self.active:
            self.func()
        else:
            print("INACTIVE BUTTON!")