# Load and initialize Modules here
import pygame
from Card import Card
from Rank import Rank
from Suit import Suit
from Button import Button
from GraphicButton import GraphicButton
from random import random

pygame.init()

buttons = []
bebas_font = pygame.font.Font('./fonts/Bebas-Regular.otf', 24)

WHITE = (255,255,255)

# Clock
windowclock = pygame.time.Clock()

# Load other things such as images and sound files here
# image = pygame.image.load("foo.png").convert # Use convert_alpha() for images with transparency
displayw = 800
displayh = 600
CARD_SIZE = (142,200)
window = pygame.display.set_mode((displayw,displayh))
player_y_pos = window.get_height() * .9 - CARD_SIZE[1]


# Main Class
class blackjack_view(object):
    # Window Information

    def __init__(self,displayw,displayh):
        self.dw = displayw
        self.dh = displayh
        self.initializeButtons()
        self.main()
    
    def hit(self):
        print("HIT!")

    def initializeButtons(self):
        # chip_img = chip.getMediumSizedGraphic()
        #     chip_btn = GraphicButton(chip_img, lambda: chip_action(chip.value), getChipPosition(chip))
        #     buttons.append(chip_btn)
        
        hit_img = pygame.image.load("images/buttons/hit.png")
        scaled_hit = pygame.transform.scale(hit_img, (int(hit_img.get_width()/4),int(hit_img.get_height()/4)))
        hit_btn = GraphicButton(scaled_hit, lambda: print("HIT!"), (window.get_width() * .9, window.get_height() * .7)) # "images/hit", hit, (win.get_width() * .9, win.get_height() * .7))
        # hit_btn.setIsVisible(True) # NEEDS TO BE IMPLEMENTED
        buttons.append(hit_btn)

        # TODO: Create all other buttons in the same way as above...

        # stand_btn = Button("Stand", stand, (win.get_width() * .85, win.get_height() * .85))
        # buttons.append(stand_btn)

        # double_down_btn = Button("Double\nDown", doubleDown, (win.get_width() * .75, win.get_height() * .7))
        # buttons.append(double_down_btn)

        # split_btn = Button("Split", split, (win.get_width() * .7, win.get_height() * .85))
        # buttons.append(split_btn)

        # bet_again_btn = Button("Bet Again", betAgain, (win.get_width() * .9, win.get_height() * .1))
        # buttons.append(bet_again_btn)

        # insurance_btn = Button("Insurance?", insurance, (win.get_width() * .9, win.get_height() * .25))
        # insurance_btn.setIsVisible(False)
        # buttons.append(insurance_btn)

        # deal_btn = Button("Deal", deal, (win.get_width() * .9, win.get_height() * .4))
        # deal_btn.setIsVisible(True)
        # buttons.append(deal_btn)

    def render_bg(self):
        window.fill((0,0,0)) #Tuple for filling display... Current is white
        bg = pygame.image.load("images/other/green_table.png") #1.54 aspect ratio
        bg_to_fit = pygame.transform.scale(bg, (self.dw, int(self.dw/1.54)))
        window.blit(bg_to_fit, (0,0))

    def render_count_label(self, count, position=3):
        label_img = pygame.image.load("images/other/count_label.png")
        label_img = pygame.transform.scale(label_img, (int(self.dw/20), int((self.dw/20)*1)))
        x_offset = 12
        if (count < 10):
            x_offset += 3
        
        anchor_pos = self.get_count_label_pos()

        window.blit(label_img, self.get_count_label_pos())

        score_label = bebas_font.render(str(count), 1, WHITE)
        window.blit(score_label, (anchor_pos[0]+x_offset, anchor_pos[1]+5))

    def render_player_cards(self, hand):
        # drawPlayerLabel() # MOVE IN FROM blackjack.py
        # drawBetLabel()    # MOVE IN FROM blackjack.py

        for i in range(len(hand)):
            self.render_card(card=hand[i], pos=self.get_card_pos(i, False))
    
    # MOVE TO Card.py ?
    def render_card(self, card, pos):
        card_img = card.getSmallSizedGraphic()
        window.blit(card_img, (int(pos[0]),int(pos[1])))

    # MOVE TO Chip.py ?
    # def render_chip(chip):
    #     chip_img = chip.getMediumSizedGraphic()
    #     chip_btn = GraphicButton(chip_img, lambda: chip_action(chip.value), getChipPosition(chip))
    #     # buttons.append(chip_btn)

    # MOVE TO Chip.py ?
    # def render_negative_chip(chip):
    #     chip_img = chip.getSmallGraphic()
    #     chip_btn = GraphicButton(chip_img, lambda: chip_reduce(chip.value), getNegChipPosition(chip))
    #     # buttons.append(chip_btn)

    def get_count_label_pos(self, position=3):
        x = window.get_width() / 2 - 50
        y = player_y_pos - 50
        return (int(x),int(y))

    def get_card_pos(self, card_num, is_dealer=True):
        x = ((window.get_width() / 2) - (50 + card_num*20)) + card_num*40
        # y = dealer_y_pos
        # if (not is_dealer):
        y = player_y_pos # + (random()*40-20) 
        # -- TODO: Add random y and rot shift. Can't do it here, it changes every frame
        #       -- Maybe move to Card.py, and initialize it there when creating a new card?
        return (int(x),int(y))
    
    def drawButtons(self):
        # window.blit(buttons[0], (100,100))# (int(pos[0]),int(pos[1])))
        for b in buttons:
            b.draw(window)
    
    def main(self):
        #Put all variables up here
        stopped = False

        while stopped == False:
            hand = [Card(Rank.TWO, Suit.CLUBS, True), Card(Rank.KING, Suit.SPADES, True), Card(Rank.THREE, Suit.DIAMONDS, True)]
            self.render_bg()
            self.render_player_cards(hand)
            self.render_count_label(9)
            self.drawButtons()

            #Event Tasking
            #Add all your event tasking things here
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    stopped = True

            #Add things like player updates here
            #Also things like score updates or drawing additional items
            # Remember things on top get done first so they will update in the order yours is set at

            # Remember to update your clock and display at the end
            pygame.display.update()
            windowclock.tick(60)

        # If you need to reset variables here
        # This includes things like score resets

    # After your main loop throw in extra things such as a main menu or a pause menu
    # Make sure you throw them in your main loop somewhere where they can be activated by the user

# All player classes and object classes should be made outside of the main class and called inside the class
#The end of your code should look something like this
if __name__ == "__main__":
    blackjack_view(displayw, displayh)
