from enum import Enum
# Using enum class create enumerations
class Rank(Enum):
    ACE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5
    SIX = 6
    SEVEN = 7
    EIGHT = 8
    NINE = 9
    TEN = 10
    JACK = 11
    QUEEN = 12
    KING = 13

    def __str__(self):
        return str(self.name)
    
    def get_display(self):
        if self.name == 'ACE':
            return 'A'
        elif self.name == 'TWO':
            return '2'
        elif self.name == 'THREE':
            return '3'
        elif self.name == 'FOUR':
            return '4'
        elif self.name == 'FIVE':
            return '5'
        elif self.name == 'SIX':
            return '6'
        elif self.name == 'SEVEN':
            return '7'
        elif self.name == 'EIGHT':
            return '8'
        elif self.name == 'NINE':
            return '9'
        elif self.name == 'TEN':
            return '10'
        elif self.name == 'JACK':
            return 'J'
        elif self.name == 'QUEEN':
            return 'Q'
        elif self.name == 'KING':
            return 'K'
        