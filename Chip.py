import pygame
from enum import Enum
class Chip(Enum):
    CHIPS_50 = 50
    CHIPS_100 = 100
    CHIPS_500 = 500
    CHIPS_1000 = 1000
    CHIPS_5000 = 5000
    
    @classmethod
    def _missing_(cls, value):
        # if isinstance(value, date):
        #     return cls(value.weekday())
        return super()._missing_(value)
        
    def __str__(self):
        return str(self.name)
    
    def getFullSizedGraphic(self):
        return pygame.image.load("images/chips/"+str(self.name)+".png")
        # return self.graphic

    def getMediumSizedGraphic(self):
        graphic = pygame.image.load("images/chips/"+str(self.name)+".png")
        # graphic = self.graphic
        med_graphic = pygame.transform.scale(graphic, (75,85))
        return med_graphic

    def getSmallGraphic(self):
        graphic = pygame.image.load("images/chips/"+str(self.name)+".png")
        sm_graphic = pygame.transform.scale(graphic, (25,28))
        return sm_graphic
    
    def get_display(self):
        if self.name == 'CHIPS_50':
            return '50'
        elif self.name == 'CHIPS_100':
            return '100'
        elif self.name == 'CHIPS_500':
            return '500'
        elif self.name == 'CHIPS_1000':
            return '1000'
        elif self.name == 'CHIPS_5000':
            return '5000'