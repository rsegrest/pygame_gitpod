from enum import Enum
# Using enum class create enumerations
class Result(Enum):
    WIN = 1
    LOSE = 2
    PUSH = 3
    BLACKJACK = 4
    INSURANCE = 5

    def __str__(self):
        return str(self.name)
    
    def __repr__(self):
        return self.__str__()